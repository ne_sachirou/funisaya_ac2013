'use strict';
var Promise = require('bluebird');

function supertest(app) {
  var request = require('supertest')(app);

  function all(tests) {
    return Promise.all(tests.map(function (test) {
      return new Promise(function (resolve, reject) {
        test(function (err) {
          if (err) { return reject(err); }
          resolve();
        });
      });
    }));
  }

  function testDoseServerStart(done) {
    request.get('/').expect(200).end(done);
  }

  function test404WithInvalidIdToShowAnsuz1Member(done) {
    request.get('/ansuz/a/member').expect(404).end(done);
  }

  function test404WithInvalidWhoToShowAnsuz1Member(done) {
    request.get('/ansuz/1/crain').expect(404).end(done);
  }

  function testShowAnsuz1Member(done) {
    request.get('/ansuz/1/member').expect(200).end(done);
  }

  function testShowAnsuz1FuniSaya(done) {
    request.get('/ansuz/1/funisaya').expect(200).end(done);
  }

  return all([
    testDoseServerStart,
    test404WithInvalidIdToShowAnsuz1Member,
    test404WithInvalidWhoToShowAnsuz1Member,
    testShowAnsuz1Member,
    testShowAnsuz1FuniSaya
  ]);
}

module.exports = function (grunt) {
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    jshint: {
      options: { jshintrc: '.jshintrc' },
      all: [
        '*.js',
        'routes/**/*.js',
        'lib/**/*.js',
        'public/javascripts/**/ansuz2_member.js',
        'public/javascripts/**/layout.js',
      ]
    }
  });

  grunt.loadNpmTasks('grunt-contrib-jshint');

  grunt.registerTask('supertest', function () {
    var app = require('./app');
    var done = this.async();

    supertest(app).
      then(function () { done(); }).
      catch(function (err) { done(err); });
  });

  grunt.registerTask('test', ['jshint', 'supertest']);
};
