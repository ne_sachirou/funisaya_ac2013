'use strict';
var http = require('http'),
    path = require('path');
var Promise = require('bluebird'),
    bodyParser = require('body-parser'),
    express = require('express'),
    passport = require('passport'),
    Knex = require('knex');
var routes = require('./routes'),
    authRoutes = require('./routes/auth'),
    UserRepository = require('./lib/repository/user_repository');
var app = express(),
    config = require('./config')[app.get('env') || 'development'];

/**
 * @param {Object} config
 * @param {Knex} db
 * @return {Promise}
 */
function updateUsersTwitter(config, db) {
  var repository = new UserRepository(db),
      consumerKey = config.twitter.consumerKey,
      consumerSecret = config.twitter.consumerSecret;

  return repository.all().then(function (users) {
    return Promise.all(users.map(function (user) {
      return user.updateTwitter(consumerKey, consumerSecret).then(function (user) {
        return repository.update(user);
      });
    }));
  });
}

app.set('port', process.env.PORT || 3000);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.use(require('morgan')('dev'));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(require('method-override')());
app.use(require('cookie-parser')('funisaya-ansuz'));
app.use(require('express-session')({
  resave:            true,
  saveUninitialized: true,
  secret:            'c4se',
}));
app.use(passport.initialize());
app.use(passport.session());
app.use(require('less-middleware')(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'public')));
app.set('db', Knex.initialize(config.db));
if ('development' === app.get('env')) {
  app.use(require('errorhandler')());
}

routes.init(app.get('db'));
authRoutes.init(config, app.get('db'));
app.use('/', routes.router);
app.use('/auth', authRoutes.router);
app.get('/schedule', function (req, res) { res.redirect('/'); });
app.get('/ansuz2/kyutest', function (req, res) { res.redirect('/aunsuz/2/member'); });
app.get('/ansuz6/empties', function (req, res) { res.redirect('/ansuz/6/member'); });
app.get('/ansuz9/izonmoji', function (req, res) { res.redirect('/ansuz/9/member'); });

http.createServer(app).listen(app.get('port'), function () {
  console.log('Express server listening on port ' + app.get('port'));
});
updateUsersTwitter(config, app.get('db'));

module.exports = app;
