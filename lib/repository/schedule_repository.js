'use strict';

var Promise = require('bluebird');
var Repository = require('./repository'),
    Schedule = require('../model/schedule');

function ScheduleRepository() {
  Repository.apply(this, arguments);
  this.model = Schedule;
}

ScheduleRepository.prototype = Object.create(Repository.prototype);
ScheduleRepository.prototype.constructor = ScheduleRepository;

ScheduleRepository.prototype.all = function () {
  /* jshint sub:false, camelcase:false */
  var schedules;

  schedules = [
    {
      id: 1,
      week_number: 1,
      week_str: '2013-12-30 - 2014-01-12',
      user_id: 1,
      user_dummy_name: 'NatsuNatsu',
      user_title: 'Foreplay to Darkness',
      funisaya_title: 'No.1 Recycle〈隷属〉'
    },
    {
      id: 2,
      week_number: 2,
      week_str: '2014-01-13 - 2014-01-26',
      user_id: 2,
      user_dummy_name: 'ｷｭｯﾁｬﾝ',
      user_title: 'ｷｭｯﾁｬﾝFPS',
      funisaya_title: '対考Naudiz'
    },
    {
      id: 3,
      week_number: 3,
      week_str: '2014-01-27 - 2014-02-09',
      user_id: 3,
      user_dummy_name: 'ryusei',
      user_title: 'telephoneNoService',
      funisaya_title: '濫DNAウォーク123'
    },
    {
      id: 4,
      week_number: 4,
      week_str: '2014-02-10 - 2014-02-23',
      user_id: 4,
      user_dummy_name: 'supermomonga',
      user_title: 'elastic hive',
      funisaya_title: 'ゲーボ-ギョーフ乱舞'
    },
    {
      id: 5,
      week_number: 5,
      week_str: '2014-02-24 - 2014-03-09',
      user_id: 5,
      user_dummy_name: '三井',
      user_title: 'ふぁふぁももんが',
      funisaya_title: '未練を捨てよ'
    },
    {
      id: 6,
      week_number: 6,
      week_str: '2014-03-10 - 2014-03-23',
      user_id: 6,
      user_dummy_name: 'Solphy',
      user_title: 'empties',
      funisaya_title: '拒絶生物の到来'
    },
    {
      id: 7,
      week_number: 7,
      week_str: '2014-03-24 - 2014-04-06',
      user_id: 7,
      user_dummy_name: 'Takuya Namba',
      user_title: 'Momonglized (not-noise ver)',
      funisaya_title: '依存性気体ワイファイ'
    },
    {
      id: 8,
      week_number: 8,
      week_str: '2014-04-07 - 2014-04-20',
      user_id: 8,
      user_dummy_name: '呉川良融',
      user_title: 'ワルツモドキ',
      funisaya_title: 'ペオーズ・無意味をくぐり抜けよ'
    },
    {
      id: 9,
      week_number: 9,
      week_str: '2014-04-21 - 2014-05-04',
      user_id: 9,
      user_dummy_name: '依存文字',
      user_title: '切り絵',
      funisaya_title: '復た繰り返す夢の人'
    },
    {
      id: 10,
      week_number: 10,
      week_str: '2014-05-05 - 2014-05-18',
      user_id: 10,
      user_dummy_name: '魔法少女14才',
      user_title: '箱を所有するところの欲',
      funisaya_title: '非植物性灌木Y'
    }
  ];
  return new Promise(function (resolve) { resolve(schedules); });
};

module.exports = ScheduleRepository;
