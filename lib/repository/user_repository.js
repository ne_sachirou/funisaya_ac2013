'use strict';

var Repository = require('./repository'),
    User = require('../model/user');

function UserRepository() {
  Repository.apply(this, arguments);
  this.model = User;
}

UserRepository.prototype = Object.create(Repository.prototype);
UserRepository.prototype.constructor = UserRepository;

UserRepository.prototype.findByTwitterId = function (twitterId) {
  var me = this,
      schema = new this.model().schema;

  return this.db(schema.name).
    where('twitter_id', twitterId).
    column(Object.keys(schema.fields)).
    select().
    then(function (resp) {
      if (!resp[0]) { return null; }
      return new me.model().fromFields(resp[0]);
    });
};

/**
 * @param {string} twitterToken
 * @param {string} twitterTokenSecret
 * @param {Object} twitterProfile
 * @return {User}
 */
UserRepository.prototype.findOrCreateByTwitter = function (twitterToken, twitterTokenSecret, twitterProfile) {
  var _this = this,
      twitterId, twitterUserName;

  twitterId = twitterProfile.id;
  twitterUserName = twitterProfile.username;
  twitterProfile = twitterProfile._json;
  return this.findByTwitterId(twitterId).then(function (user) {
    /* jshint sub:false, camelcase:false */
    if (!user) {
      _this.count().then(function (count) {
        if (count >= 10) { throw new Error('Too many users.'); }
        user = new User().fromFields({
          twitter_token:        twitterToken,
          twitter_token_secret: twitterTokenSecret,
          twitter_id:           twitterId,
          twitter_user_name:    twitterUserName,
          twitter_profile:      twitterProfile
        });
        return _this.insert(user);
      });
    }
    user.twitterToken = twitterToken;
    user.twitterTokenSecret = twitterTokenSecret;
    user.twitterProfile = twitterProfile;
    return _this.update(user);
  });
};

module.exports = UserRepository;
