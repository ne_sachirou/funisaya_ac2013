'use strict';

var Promise = require('bluebird');

function upgradeField(db, schema, table, fieldName) {
  var field = schema.fields[fieldName],
      type = field[0],
      option = field[1] || {};

  function createField(table, fieldName, type, option) {
    switch (type) {
    case 'serial':
      return table.increments(fieldName);
    case 'string':
      return table.string(fieldName, option.length || void 0);
    case 'text':
      return table.text(fieldName, option.length || void 0);
    case 'json':
      return table.json(fieldName);
    case 'integer':
      return table.integer(fieldName);
    }
  }

  function addConstraint(field, option) {
    if (option.primary) { field = field.primary(); }
    if (option.unique) { field = field.unique(); }
    if (option['default']) { field = field.defaltTo(option['default']); }
    return field;
  }

  return db.schema.hasColumn(schema.name, fieldName).then(function (isExist) {
    var field;

    if (isExist) { return; }
    field = createField(table, fieldName, type, option);
    addConstraint(field, option);
  });
}

function upgrade(repository, table) {
  var db = repository.db,
      schema = new repository.model().schema;

  return Promise.all(Object.keys(schema.fields).map(function (fieldName) {
    return upgradeField(db, schema, table, fieldName);
  })).then(function () { return repository; });
}

function Repository(db) {
  this.db = db;
  this.model = null;
}

Repository.prototype.migrate = function (callback) {
  var _this = this,
      schema = new this.model().schema;

  this.db.schema.hasTable(schema.name).then(function (isExist) {
    var promise;

    function onTable(table) {
      upgrade(_this, table).then(function () {
        promise.then(function () { callback(); }, callback);
      });
    }

    if (isExist) {
      promise = _this.db.schema.table(schema.name, onTable);
    } else {
      promise = _this.db.schema.createTable(schema.name, onTable);
    }
  }, callback);
  return this;
};

Repository.prototype.all = function () {
  var _this = this,
      schema = new this.model().schema;

  return this.db(schema.name).
    column(Object.keys(schema.fields)).
    orderBy('id', 'ASC').
    select().
    then(function (resp) {
      resp = resp.map(function (fields) {
        return new _this.model().fromFields(fields);
      });
      return resp;
    });
};

Repository.prototype.count = function () {
  var schema = new this.model().schema;

  return this.db(schema.name).count('id');
};

Repository.prototype.findById = function (id) {
  var _this = this,
      schema = new this.model().schema;

  return this.db(schema.name).
    where('id', id).
    column(Object.keys(schema.fields)).
    select().
    then(function (resp) {
      if (!resp[0]) { return null; }
      return new _this.model().fromFields(resp[0]);
    });
};

Repository.prototype.insert = function (model) {
  var fields = model.toFields(),
      schema = new this.model().schema;

  delete fields.id;
  return this.db(schema.name).
    insert(fields, 'id').
    then(function (id) {
      model.id = id[0];
      return model;
    });
};

Repository.prototype.update = function (model) {
  var fields = model.toFields(),
      schema = new this.model().schema;

  delete fields.id;
  return this.db(schema.name).
    where('id', model.id).
    update(fields).
    then(function () { return model; });
};

module.exports = Repository;
