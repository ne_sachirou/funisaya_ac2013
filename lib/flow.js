/**
 * @description Minimal async flow controller. Like uupaa's Flow.js https://github.com/uupaa/flow.js
 * @author ne_Sachirou <utakata.c4se@gmail.com>
 * @license Public Domain
 */

(function (scope) {
  'use strict';

  /**
   * @example
   * var flow = new Flow(2, function (error, passes) {
   *   if (error) { throw error; }
   *   passes.forEach(function (result) {
   *     console.log(result);
   *   });
   * });
   * doDalayFunc1(function (error, result) {
   *   if (error) {
   *     flow.miss(error);
   *     return;
   *   }
   *   flow.pass(result);
   * });
   * doDalayFunc2(function (error) {
   *   if (error) {
   *     flow.miss(error);
   *     return;
   *   }
   *   flow.pass();
   * });
   *
   * @class Flow
   * @name Flow
   * @constructor
   * @param {number} waits
   * @param {function(?Error,?Array.<Object>)} callback callback(err, passes)
   * @param {boolean=} isMissable =false
   */
  function Flow(waits, callback, isMissable) {
    this.waits = waits;
    this.callback = callback;
    this.isMissable = !! isMissable;
    /* @type {boolean} */
    this.isFinished = false;
    /* @type {Array.<Object>} */
    this.passes = [ ];
    Flow.flows.push(this);
  }

  /**
   * @name Flow.flows
   * @type {Array.<Flow>}
   * */
  Flow.flows = [ ];

  /**
   * @name Flow.pass
   * @function
   * @param {Object} value
   * @param {string=} key
   * @return {Flow}
   */
  Flow.prototype.pass = function (value, key) {
    if (this.isFinished) { return; }
    if (key) {
      this.passes[key] = value;
    } else {
      this.passes.push(value);
    }
    check(this);
    return this;
  };

  /**
   * @name Flow.miss
   * @function
   * @param {Error} err
   * @return {Flow}
   */
  Flow.prototype.miss = function (err) {
    if (this.isFinished) { return; }
    if (! this.isMissable) {
      finalize(this);
      this.callback(err, null);
    } else {
      check(this);
    }
    return this;
  };

  function check(flow) {
    flow.waits -= 1;
    if (flow.waits <= 0) {
      finalize(flow);
      flow.callback(null, flow.passes);
    }
  }

  function finalize(flow) {
    flow.isFinished = true;
    Flow.flows = Flow.flows.filter(function (elm) { return elm !== flow; });
  }

  if (module) {
    module.exports = Flow;
  } else {
    scope.Flow = Flow;
  }

}(this.self || global));
