'use strict';

var Model = require('./model');

function Schedule() {
  /* jshint sub:false, camelcase:false */
  this.schema = {
    name: 'schedule',
    fields: {
      id:              ['serial'],
      week_number:     ['integer'],
      week_str:        ['string'],
      user_id:         ['integer'],
      user_dummy_name: ['string'],
      user_title:      ['string'],
      funisaya_title:  ['string']
    }
  };
  Model.apply(this, arguments);
}

Schedule.prototype = Object.create(Model.prototype);
Schedule.prototype.constructor = Schedule;

module.exports = Schedule;
