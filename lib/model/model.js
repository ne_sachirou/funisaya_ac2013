'use strict';

/*
 * Transform underscored to camel case.
 * @param {string} name
 * @return {string}
 */
function toCamelCase(name) {
  return name.replace(/([^_])_([^_])/g, function (str, p1, p2) {
    return p1 + p2.toUpperCase();
  });
}

function Model() {
  Object.keys(this.schema.fields).forEach(function (fieldName) {
    this[toCamelCase(fieldName)] =
      (this.schema.fields[fieldName][1] || {})['default'] || null;
  }, this);
}

Model.prototype.fromFields = function (fields) {
  Object.keys(this.schema.fields).forEach(function (fieldName) {
    if (fields[fieldName]) {
      this[toCamelCase(fieldName)] = fields[fieldName];
    }
  }, this);
  return this;
};

Model.prototype.toFields = function () {
  var fields = {};

  Object.keys(this.schema.fields).forEach(function (fieldName) {
    fields[fieldName] = this[toCamelCase(fieldName)] || null;
  }, this);
  return fields;
};

module.exports = Model;
