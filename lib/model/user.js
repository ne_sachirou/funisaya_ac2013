'use strict';

var Promise = require('bluebird'),
    Twitter = require('twitter'),
    Model = require('./model');

function User() {
  /* jshint sub:false, camelcase:false */
  this.schema = {
    name: 'user',
    fields: {
      id: ['serial'],
      twitter_token: ['string', { length: 255 }],
      twitter_token_secret: ['string', { length: 255 }],
      twitter_id: ['string', { length: 255, unique: true }],
      twitter_user_name: ['string', { length: 255 }],
      twitter_profile: ['text'],
      twitter_profile_image: ['binary']
    }
  };
  Model.apply(this, arguments);
}

User.prototype = Object.create(Model.prototype);
User.prototype.constructor = User;

User.prototype.fromFields = function (fields) {
  Model.prototype.fromFields.call(this, fields);
  if (Object.prototype.toString.call(this.twitterProfile) === '[object String]') {
    this.twitterProfile = JSON.parse(this.twitterProfile);
  }
  if (!this.twitterProfile) { this.twitterProfile = {}; }
  return this;
};

User.prototype.toFields = function () {
  /* jshint sub:false, camelcase:false */
  var fields = Model.prototype.toFields.call(this);

  fields.twitter_profile = JSON.stringify(fields.twitter_profile);
  return fields;
};

/**
 * @param {string} consumerKey
 * @param {string} consumerSecret
 * @return {Promise.<User>}
 */
User.prototype.updateTwitter = function (consumerKey, consumerSecret) {
  /* jshint sub:false, camelcase:false */
  var _this = this,
      twitter = new Twitter({
        consumer_key:        consumerKey,
        consumer_secret:     consumerSecret,
        access_token_key:    this.twitterToken,
        access_token_secret: this.twitterTokenSecret
      });

  return new Promise(function (resolve) {
    twitter.get('/account/verify_credentials.json', {}, function (profile) {
      if (profile instanceof Error) {
        console.error(profile);
        return resolve(_this);
      }
      _this.twitterId = profile.id;
      _this.twitterUserName = profile.screen_name;
      _this.twitterProfile = profile;
      resolve(_this);
    });
  });
};

module.exports = User;
