'use strict';

function migrateAll() {
  var Knex = require('knex'),
      config = require('./config')[process.env.NODE_ENV || 'development'],
      Flow = require('./lib/flow');
  var flow, db, repositories;

  function migrate(Repository, name) {
    new Repository(db).migrate(function (err) {
      if (err) { flow.miss(err); return; }
      console.log('"' + name + '" migrated.');
      flow.pass();
    });
  }

  repositories = ['user', 'schedule'];
  db = Knex.initialize(config.db);
  flow = new Flow(repositories.length, function (err) {
    if (err) { throw err; }
    process.exit();
  });
  repositories.forEach(function (repository) {
    migrate(require('./lib/repository/' + repository + '_repository'), repository);
  });
}

migrateAll();
