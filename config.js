'use strict';

var pg, client;

if (process.env.NODE_ENV === 'production') {
  pg = require('pg');
  client = new pg.Client(process.env.DATABASE_URL);
} else {
  client = {connectionParameters: {}};
}

module.exports = {
  development: {
    db: {
      client: 'sqlite',
      connection: {
        filename: __dirname + '/db.sqlite3'
      }
    },
    twitter: {
      consumerKey: process.env.TWITTER_CONSUMER_KEY,
      consumerSecret: process.env.TWITTER_CONSUMER_SECRET,
      callbackUrl: 'http://127.0.0.1:3000/auth/twitter/callback'
    }
  },
  production: {
    db: {
      client: 'pg',
      connection: {
        host: client.connectionParameters.host,
        port: client.connectionParameters.port,
        user: client.connectionParameters.user,
        password: client.connectionParameters.password,
        database: client.connectionParameters.database,
        charset: 'utf-8'
      }
    },
    twitter: {
      consumerKey: process.env.TWITTER_CONSUMER_KEY,
      consumerSecret: process.env.TWITTER_CONSUMER_SECRET,
      callbackUrl: 'http://funisaya-ac2013.heroku.com/auth/twitter/callback'
    }
  }
};
