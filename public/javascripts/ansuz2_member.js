/* global UnityObject2 */
var config = {
  width:  960,
  height: 600,
  params: { enableDebugging: '0' }
};
var filename = 'https://dl.dropboxusercontent.com/u/13307622/ansuz/ansuz2_kyutest.unity3d';
var u = new UnityObject2(config);

jQuery(function () {
  'use strict';
  var $missingScreen = jQuery('#unityPlayer').find('.missing');
  var $brokenScreen = jQuery('#unityPlayer').find('.broken');

  $missingScreen.hide();
  $brokenScreen.hide();
  u.observeProgress(function (progress) {
    switch (progress.pluginStatus) {
      case 'broken':
        $brokenScreen.find('a').click(function (evt) {
          evt.stopPropagation();
          evt.preventDefault();
          u.installPlugin();
          return false;
        });
        $brokenScreen.show();
        break;
      case 'missing':
        $missingScreen.find('a').click(function (evt) {
          evt.stopPropagation();
          evt.preventDefault();
          u.installPlugin();
          return false;
        });
        $missingScreen.show();
        break;
      case 'installed':
        $missingScreen.remove();
        break;
      case 'first':
        break;
    }
  });
  u.initPlugin(jQuery('#unityPlayer')[0], filename);
});
