(function () {
'use strict';

var SideNav = function () {
  var instance = this;

  SideNav = function () { return instance; };
  this.node = document.querySelector('.main .side-nav');
  this.toggleBtnNode = this.node.querySelector('.side-nav-toggle-btn');
};

SideNav.prototype.init = function () {
  var _this = this;

  if (window.innerWidth <= 758) {
    this.node.classList.add('side-nav-hidden');
  }
  this.toggleBtnNode.onclick = function () { _this.toggle(); };
  window.addEventListener('resize', function () {
    if (window.innerWidth <= 758) {
      _this.node.classList.remove('side-nav-hidden');
    } else {
      _this.node.classList.add('side-nav-hidden');
    }
    _this.toggle();
  });
};

SideNav.prototype.toggle = function () {
  this.node.classList.toggle('side-nav-hidden');
};

window.addEventListener('DOMContentLoaded', function () {
  new SideNav().init();
});

}());
