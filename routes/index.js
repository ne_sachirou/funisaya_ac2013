'use strict';

var fs = require('fs');
var express = require('express'),
    jade = require('jade'),
    Promise = require('bluebird');
var UserRepository = require('../lib/repository/user_repository'),
    ScheduleRepository = require('../lib/repository/schedule_repository');
var db,
    router = express.Router();

/**
 * @return {Promise.<User>}
 */
function getCurrentUser(session) {
  if (!session.passport || !session.passport.user) {
    return new Promise(function (resolve) { resolve(null); });
  }
  return new UserRepository(db).findById(session.passport.user);
}

/**
 * @return {Promise.<Object.<string,Object>>}
 */
function getSchedules() {
  return new Promise.all([
    new UserRepository(db).all(),
    new ScheduleRepository(db).all()
  ]).then(function (values) {
    var users, schedules;

    users = values[0];
    schedules = values[1];
    schedules = schedules.map(function (schedule) {
      schedule.user = users.filter(function (user) {
        return user.id === schedule.userId;
      });
      return schedule;
    });
    return { users: users, schedules: schedules };
  });
}

/**
 * @param {} session
 * @return {Promise.<Object.<string,Object>>}
 */
function prepare(session) {
  return Promise.all([getCurrentUser(session), getSchedules()]).
    then(function (values) {
      var currentUser = values[0],
          users = values[1].users || [],
          schedules = values[1].schedules || [];

      return { currentUser: currentUser, users: users, schedules: schedules };
    });
}

/**
 * @param {express.Request} req
 * @param {express.Response} res
 * @param {string} view
 * @param {Object.<string,Object>} params
 * @return {Promise.<undefined>}
 */
function doRender(req, res, view, params) {
  try {
    return prepare(req.session).then(function (values) {
      var key,
          keys = Object.keys(params),
          i = 0,
          iz = keys.length;

      for (; key = keys[i], i < iz; ++i) { values[key] = params[key]; }
      res.render(view, values);
    }).catch(function (err) {
      res.json(500, { error: err.message });
    });
  } catch (err) {
    return new Promise(function (resolve) {
      res.json(500, { error: err.message });
      resolve();
    });
  }
}

router.param('id', function (req, res, next, id) {
  if (!/^\d+$/.test(id)) { return res.status(404).end(); }
  next();
});

router.param('who', function (req, res, next, who) {
  if (!/^member|funisaya$/.test(who)) { return res.status(404).end(); }
  next();
});

router.get('/', function (req, res) {
  doRender(req, res, 'index', { title: 'ansuz' });
});

router.get('/join', function (req, res) {
  doRender(req, res, 'join', { title: '参加する' });
});

router.get('/ansuz/:id/:who', function (req, res) {
  var id = parseInt(req.params.id),
      who = req.params.who,
      contentFile = 'views/ansuz' + id + '_' + who + '.jade';

  /**
   * @param {string} filename
   * @return {Promise.<Object.<string,Object>>}
   */
  function prepareContent(filename, data) {
    return new Promise(function (resolve, reject) {
      fs.exists(filename, function (isExists) {
        if (!isExists) {
          data.work.content = 'Comming...';
          return resolve(data);
        }
        jade.renderFile(filename, data, function (err, html) {
          if (err) { return reject(err); }
          data.work.content = html;
          resolve(data);
        });
      });
    });
  }

  prepare(req.session).then(function (data) {
    /* jshint sub:false, camelcase:false */
    var schedule = data.schedules[id - 1],
        user = data.users[schedule.user_id];

    data.who = who;
    data.schedule = schedule;
    data.user = user;
    data.work = who === 'member' ? {
      title: schedule.user_title,
      dummy_name: schedule.user_dummy_name,
      content: ''
    } : {
      title: schedule.funisaya_title,
      dummy_name: 'FuniSaya',
      content: ''
    };
    data.title = data.work.title;
    return prepareContent(contentFile, data);
  }).then(function (data) {
    res.render('ansuz', data);
  }).catch(function (err) {
    res.json(500, { error: err.message });
  });
});

module.exports = {
  init: function (_db) { db = _db; },
  router: router
};
