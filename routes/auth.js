'use strict';

var express = require('express'),
    passport = require('passport'),
    TwitterStrategy = require('passport-twitter').Strategy;
var UserRepository = require('../lib/repository/user_repository');
var router = express.Router();

function init(config, db) {
  passport.use(new TwitterStrategy({
      consumerKey: config.twitter.consumerKey,
      consumerSecret: config.twitter.consumerSecret,
      callbackURL: config.twitter.callbackUrl
    },
    function (token, tokenSecret, profile, done) {
      new UserRepository(db).findOrCreateByTwitter(token, tokenSecret, profile).
        then(function (user) { done(null, user); }).
        catch(function (err) { done(err, null); });
    }
  ));
  passport.serializeUser(function (user, done) {
    done(null, user.id);
  });
  passport.deserializeUser(function (id, done) {
    new UserRepository(db).findById(id).then(function (user) {
      done(null, user);
    }).catch(function (err) { done(err, null); });
  });
}

router.get('/twitter', passport.authenticate('twitter'));

router.get('/twitter/callback', passport.authenticate('twitter', {
  successRedirect: '/',
  failureRedirect: '/join'
}));

module.exports = {
  init: init,
  router: router
};
